import 'package:flutter/cupertino.dart';
import 'package:hotel_booking_5/localization/demo_local.dart';

String getTranslated(BuildContext context, String key) {
  return DemoLocalizations.of(context).getTranslatedValues(key);
}
