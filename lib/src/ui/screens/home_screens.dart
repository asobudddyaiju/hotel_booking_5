import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/src/ui/screens/dot.dart';
import 'package:hotel_booking_5/src/ui/screens/home_block.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/localization/locale.dart';
import 'package:hotel_booking_5/src/ui/screens/destination_page.dart';
import 'package:hotel_booking_5/src/ui/screens/search_destination.dart';
import 'package:hotel_booking_5/src/ui/screens/webview.dart';
import 'package:hotel_booking_5/src/ui/widgets/calender.dart';
import 'package:hotel_booking_5/src/ui/widgets/guest_bottom_sheet.dart';
import 'package:hotel_booking_5/src/ui/widgets/home_drawer.dart';
import 'package:hotel_booking_5/src/ui/widgets/home_page_field_box.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/object_factory.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  var destination = "Select destination";
  String dates;
  String guest;
  String checkInDate;
  String checkOutDate;
  String nights;
  String rooms;
  String adults;
  String children;
  SharedPreferences _sharedPreferences;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseAuth _authfb = FirebaseAuth.instance;
  String monthInLetterCheckIn;
  String monthInLetterCheckOut;
  String checkInDay = '';
  String checkOutDay = '';
  void initState() {
    setState(() {
      monthInLetterCheckIn = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(5));
    });
    setState(() {
      monthInLetterCheckOut = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(6));
    });
    setState(() {
      checkInDay =
          ObjectFactory().getDay.getWeekDay(Hive.box('day').get(1)) != null
              ? ObjectFactory().getDay.getWeekDay(Hive.box('day').get(1))
              : '';
      print(checkInDay);
    });
    setState(() {
      checkOutDay =
          ObjectFactory().getDay.getWeekDay(Hive.box('day').get(2)) != null
              ? ObjectFactory().getDay.getWeekDay(Hive.box('day').get(2))
              : '';
      print(checkOutDay);
    });
    if (Hive.box('adult').get(5) != null) {
      setState(() {
        nights = Hive.box('adult').get(5).toString();
      });
    } else {
      nights = "No: of";
    }
    if ((Hive.box('code').get(3) != null)) {
      setState(() {
        checkInDate = checkInDay +
            "," +
            Hive.box('code').get(3).toString() +
            " " +
            monthInLetterCheckIn +
            " " +
            Hive.box('code').get(22);
      });
    } else {
      setState(() {
        checkInDate = "Checkin";
      });
    }
    if (Hive.box('code').get(6) != null) {
      setState(() {
        checkOutDate = checkOutDay +
            "," +
            Hive.box('code').get(4).toString() +
            " " +
            monthInLetterCheckOut +
            " " +
            Hive.box('code').get(23);
      });
    } else {
      setState(() {
        checkOutDate = "Checkout";
      });
    }

    if (Hive.box('adult').get(1) != null) {
      setState(() {
        rooms = Hive.box('adult').get(1) + " Room";
      });
    } else {
      setState(() {
        rooms = "";
      });
    }
    if (Hive.box('adult').get(2) != null) {
      setState(() {
        adults = Hive.box('adult').get(2) + " Adult";
      });
    } else {
      setState(() {
        adults = "";
      });
    }
    if (Hive.box('adult').get(3) != null) {
      setState(() {
        children = Hive.box('adult').get(3) + " Children";
      });
    } else {
      setState(() {
        children = "";
      });
    }
    if ((Hive.box('adult').get(1) == null) &&
        (Hive.box('adult').get(3) == null) &&
        (Hive.box('adult').get(3) == null)) {
      setState(() {
        guest = "Select guest";
      });
    } else {
      setState(() {
        guest = "no value";
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      drawer: HomeDrawer(auth: _auth, authfb: _authfb),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: Align(
                alignment: Alignment.topLeft,
                child: GestureDetector(
                  onTap: () {
                    _globalKey.currentState.openDrawer();
                  },
                  child: Container(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: screenWidth(context, dividedBy: 15),
                              top: screenHeight(context, dividedBy: 12)),
                          child: Container(
                            height: screenHeight(context, dividedBy: 150),
                            width: screenWidth(context, dividedBy: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Color(0xFF1B76A9),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: screenWidth(context, dividedBy: 30),
                              top: screenHeight(context, dividedBy: 75)),
                          child: Container(
                            height: screenHeight(context, dividedBy: 150),
                            width: screenWidth(context, dividedBy: 15),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Color(0xFF1B76A9),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: screenHeight(context, dividedBy: 15)),
              child: Align(
                alignment: Alignment.topLeft,
                child: Container(
                  height: screenHeight(context, dividedBy: 6),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: screenWidth(context, dividedBy: 500)),
                        child: Text(
                          'Search ',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 28,
                              fontFamily: 'Gilroy'),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: screenWidth(context, dividedBy: 12)),
                        child: Text(
                          'hotels..',
                          style: TextStyle(
                              color: Color(0xFF1B76A9),
                              fontWeight: FontWeight.bold,
                              fontSize: 40,
                              fontFamily: 'Nexa'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: screenHeight(context, dividedBy: 50)),
              child: Container(
                  child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchDestination()),
                  );
                },
                child: Field(
                  label: 'Destination',
                  icon: 'assets/icons/map-pin.svg',
                  data: Hive.box('lang').get(10) != null
                      ? Hive.box('lang').get(10)
                      : "Select destination",
                ),
              )),
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: screenHeight(context, dividedBy: 50)),
              child: Container(
                  child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DatePicker()),
                  );
                },
                child: Field(
                  label: 'Check-in',
                  icon: 'assets/icons/calendar.svg',
                  data: checkInDate != null ? checkInDate : "Select Date",
                ),
              )),
            ),
            Dot(),
            Padding(
              padding: EdgeInsets.only(top: 0),
              child: Container(
                  child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DatePicker()),
                  );
                },
                child: Field(
                  label: 'Check-out',
                  icon: 'assets/icons/calendar.svg',
                  data: checkOutDate != null ? checkOutDate : "Select Date",
                ),
              )),
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: screenHeight(context, dividedBy: 50)),
              child: Container(
                  height: screenHeight(context, dividedBy: 12),
                  width: screenWidth(context, dividedBy: 1),
                  child: Row(
                    children: [
                      Padding(
                          padding: EdgeInsets.only(top: 0),
                          child: Container(
                            height: screenHeight(context, dividedBy: 15),
                            width: screenWidth(context, dividedBy: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(30),
                                  bottomRight: Radius.circular(30)),
                              color: Constants.kitGradients[12],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                height: screenHeight(context, dividedBy: 50),
                                width: screenWidth(context, dividedBy: 50),
                                child: SvgPicture.asset(
                                  'assets/icons/Group.svg',
                                  height: screenHeight(context, dividedBy: 50),
                                  width: screenWidth(context, dividedBy: 50),
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          )),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 15),
                        width: screenWidth(context, dividedBy: 10),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Container(
                                height: screenHeight(context, dividedBy: 35),
                                width: screenWidth(context, dividedBy: 4),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                child: Text(
                                  'Guests',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontFamily: 'poppins',
                                    color: Constants.kitGradients[4],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                top: screenHeight(context, dividedBy: 500)),
                            child: Container(
                              height: screenHeight(context, dividedBy: 24),
                              width: screenWidth(context, dividedBy: 1.5),
                              child: GestureDetector(
                                onTap: () {
                                  Future(() => showModalBottomSheet(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(50),
                                              topLeft: Radius.circular(50))),
                                      context: context,
                                      isScrollControlled: true,
                                      builder: (context) {
                                        return ModalBottomSheet();
                                      }));
                                },
                                child: Text(
                                  guest == "no value"
                                      ? "$rooms" +
                                          ", " +
                                          "$adults" +
                                          ", " +
                                          "$children"
                                      : "Select guest",
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'poppins',
                                    color: Color(0xFF4D4D4D),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  )),
            ),
            Padding(
                padding: EdgeInsets.only(
                    top: screenHeight(context, dividedBy: 8),
                    bottom: screenHeight(context, dividedBy: 75)),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    height: screenHeight(context, dividedBy: 12),
                    width: screenWidth(context, dividedBy: 1.5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          bottomRight: Radius.circular(30)),
                      color: Constants.kitGradients[12],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Container(
                          height: screenHeight(context, dividedBy: 50),
                          width: screenWidth(context, dividedBy: 50),
                          child: Center(
                            child: Text(
                              'Search',
                              style: TextStyle(
                                  fontFamily: 'poppins',
                                  fontSize: 22,
                                  fontWeight: FontWeight.w500,
                                  color: Constants.kitGradients[1]),
                            ),
                          )),
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
