import 'package:flutter/cupertino.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class Dot extends StatefulWidget {
  @override
  _DotState createState() => _DotState();
}

class _DotState extends State<Dot> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 25),
      child: Align(
        alignment: Alignment.topLeft,
        child: Column(children: [
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight(context, dividedBy: 250),
                left: screenWidth(context, dividedBy: 20)),
            child: Container(
              height: screenHeight(context, dividedBy: 125),
              width: screenWidth(context, dividedBy: 75),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Constants.kitGradients[4],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight(context, dividedBy: 250),
                left: screenWidth(context, dividedBy: 20)),
            child: Container(
              height: screenHeight(context, dividedBy: 125),
              width: screenWidth(context, dividedBy: 75),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Constants.kitGradients[4],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight(context, dividedBy: 250),
                left: screenWidth(context, dividedBy: 20)),
            child: Container(
              height: screenHeight(context, dividedBy: 125),
              width: screenWidth(context, dividedBy: 75),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Constants.kitGradients[4],
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
