import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking_5/src/bloc/user_bloc.dart';
import 'package:hotel_booking_5/src/models/destination_list_response.dart';
import 'package:hotel_booking_5/src/ui/widgets/destination_list_tile.dart';
import 'package:hotel_booking_5/src/ui/widgets/signin_button.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class DestinationPage extends StatefulWidget {
  @override
  _DestinationPageState createState() => _DestinationPageState();
}

class _DestinationPageState extends State<DestinationPage> {
  TextEditingController destinationTextEditingController =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 12)),
          // here the desired height
          child: AppBar(
            title: Text(
              'Destination',
              style: TextStyle(color: Colors.black),
            ),
            leading: new IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
            ),
            backgroundColor: Colors.white,
            elevation: 0.5,
          )),
      body: StreamBuilder<DestinationListResponse>(
          stream: userBlocSingle.destinationResponse,
          builder: (context, snapshot) {
            return ListView(
              children: <Widget>[
                Padding(
                  padding:
                  const EdgeInsets.only(left: 30.0, right: 30.0, top: 20.0),
                  child: Container(
                    height: screenHeight(context, dividedBy: 15),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(50.0),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[300],
                              blurRadius: 1.0,
                              spreadRadius: 0.2)
                        ]),
                    child: SimpleAutoCompleteTextField(
                      controller: destinationTextEditingController,
                      textSubmitted: (_) {
                        print(destinationTextEditingController.text);
                        print("hdsdj");
                      },
                      textChanged: (_) {
                        print(destinationTextEditingController.text);
                        print("aaaa");
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.location_on),
                        hintText: 'Da Nang, Viet Nam',
                        suffixIcon: GestureDetector(
                          child: Icon(Icons.close),
                          onTap: () {
                            destinationTextEditingController.text = "";
                          },
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(150),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.all(16),
                      ),
                    ),
                  ),
                ),
                DListTile(
                  iconData: (Icons.location_city),
                  listText: 'London, England, United Kingdom',
                ),
                DListTile(
                  iconData: (Icons.flight_takeoff),
                  listText: ('London, Heathrow Airport, London'),
                ),
                DListTile(
                  iconData: (Icons.hotel),
                  listText: ('Londoner Hotel Gwangan, Vusan'),
                ),
                DListTile(
                  iconData: (Icons.flag),
                  listText: ('Long island, New York State'),
                ),
                ListTile(
                  trailing: MaterialButton(
                    color: Colors.transparent,
                    child: Text(
                      'Clear',
                      style: TextStyle(
                        color: Constants.kitGradients[0],
                      ),
                    ),
                  ),
                  title: Text(
                    'Recent Location',
                    style: TextStyle(
                        fontWeight: FontWeight.w500, color: Colors.black),
                  ),
                ),
                DListTile(
                  iconData: (Icons.hotel),
                  listText: ('Londoner Hotel Gwangan, Vusan'),
                ),
                DListTile(
                  iconData: (Icons.flag),
                  listText: 'Long island, New York State',
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 10),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 10),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Sign in to access recent searches anywhere',
                          style: TextStyle(color: Colors.black, fontSize: 10.0),
                        ),
                        SignButton()
                      ]),
                )
              ],
            );
          }),
    );
  }
}
