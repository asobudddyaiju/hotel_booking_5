// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:hive/hive.dart';
// import 'package:hotel_booking_1/localization/locale.dart';
// import 'package:hotel_booking_1/src/ui/screens/destination_page.dart';
// import 'package:hotel_booking_1/src/ui/screens/search_destination.dart';
// import 'package:hotel_booking_1/src/ui/screens/webview.dart';
// import 'package:hotel_booking_1/src/ui/widgets/calender.dart';
// import 'package:hotel_booking_1/src/ui/widgets/guest_bottom_sheet.dart';
// import 'package:hotel_booking_1/src/ui/widgets/home_drawer.dart';
// import 'package:hotel_booking_1/src/ui/widgets/home_page_field_box.dart';
// import 'package:hotel_booking_1/src/utils/constants.dart';
// import 'package:hotel_booking_1/src/utils/object_factory.dart';
// import 'package:hotel_booking_1/src/utils/utils.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// class HomePage extends StatefulWidget {
//   @override
//   _HomePageState createState() => _HomePageState();
// }
//
// class _HomePageState extends State<HomePage>
//     with SingleTickerProviderStateMixin {
//   GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
//   var destination = "Select destination";
//   String dates;
//   String guest;
//   String checkInDate;
//   String checkOutDate;
//   String nights;
//   String rooms;
//   String adults;
//   String children;
//   SharedPreferences _sharedPreferences;
//   final FirebaseAuth _auth = FirebaseAuth.instance;
//   final FirebaseAuth _authfb = FirebaseAuth.instance;
//   String monthInLetterCheckIn;
//   String monthInLetterCheckOut;
//   String checkInDay;
//   String checkOutDay;
//   AnimationController animationController;
//   Animation animationSize;
//
//   void getSharedPref() async {
//     _sharedPreferences = await SharedPreferences.getInstance();
//   }
//
//   @override
//   void initState() {
//     setState(() {
//       monthInLetterCheckIn = ObjectFactory()
//           .getMonthAlpha
//           .getCheckInMonth(Hive.box('code').get(5));
//     });
//     setState(() {
//       monthInLetterCheckOut = ObjectFactory()
//           .getMonthAlpha
//           .getCheckInMonth(Hive.box('code').get(6));
//     });
//     setState(() {
//       checkInDay = ObjectFactory().getDay.getWeekDay(Hive.box('day').get(1));
//       print(checkInDay);
//     });
//     setState(() {
//       checkOutDay = ObjectFactory().getDay.getWeekDay(Hive.box('day').get(2));
//       print(checkOutDay);
//     });
//     if (Hive.box('adult').get(5) != null) {
//       setState(() {
//         nights = Hive.box('adult').get(5).toString();
//       });
//     } else {
//       nights = "No: of";
//     }
//     if ((Hive.box('code').get(3) != null)) {
//       setState(() {
//         checkInDate = checkInDay +
//             "," +
//             Hive.box('code').get(3).toString() +
//             " " +
//             monthInLetterCheckIn +
//             " " +
//             Hive.box('code').get(22);
//       });
//     } else {
//       setState(() {
//         checkInDate = "Checkin";
//       });
//     }
//     if (Hive.box('code').get(6) != null) {
//       setState(() {
//         checkOutDate = checkOutDay +
//             "," +
//             Hive.box('code').get(4).toString() +
//             " " +
//             monthInLetterCheckOut +
//             " " +
//             Hive.box('code').get(23);
//       });
//     } else {
//       setState(() {
//         checkOutDate = "Checkout";
//       });
//     }
//
//     if (Hive.box('adult').get(1) != null) {
//       setState(() {
//         rooms = Hive.box('adult').get(1) + " Room";
//       });
//     } else {
//       setState(() {
//         rooms = "";
//       });
//     }
//     if (Hive.box('adult').get(2) != null) {
//       setState(() {
//         adults = Hive.box('adult').get(2) + " Adult";
//       });
//     } else {
//       setState(() {
//         adults = "";
//       });
//     }
//     if (Hive.box('adult').get(3) != null) {
//       setState(() {
//         children = Hive.box('adult').get(3) + " Children";
//       });
//     } else {
//       setState(() {
//         children = "";
//       });
//     }
//     if ((Hive.box('adult').get(1) == null) &&
//         (Hive.box('adult').get(3) == null) &&
//         (Hive.box('adult').get(3) == null)) {
//       setState(() {
//         guest = "Select guest";
//       });
//     } else {
//       setState(() {
//         guest = "no value";
//       });
//     }
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       key: _globalKey,
//       resizeToAvoidBottomPadding: false,
//       drawer: HomeDrawer(auth: _auth, authfb: _authfb),
//       body: Container(
//         width: MediaQuery.of(context).size.width,
//         height: MediaQuery.of(context).size.height,
//         child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
//           Padding(
//             padding: EdgeInsets.only(
//                 top: screenHeight(context, dividedBy: 12), left: 20),
//             child: GestureDetector(
//               child: Container(
//                 child: Icon(
//                   Icons.short_text,
//                   size: 50,
//                   color: Colors.black,
//                 ),
//               ),
//               onTap: () {
//                 _globalKey.currentState.openDrawer();
//               },
//             ),
//           ),
// Padding(
//   padding: EdgeInsets.only(top: screenHeight(context, dividedBy: 50)),
//   child: Row(
//     children: [
// Padding(
//   padding: const EdgeInsets.only(left: 30),
//   child: Text(
//     'Search ',
//     style: TextStyle(
//         color: Colors.black,
//         fontWeight: FontWeight.w500,
//         fontSize: 26),
//             ),
//           ),
//           Text(
//             'Hotels',
//             style: TextStyle(
//                 color: Color(0xFF1B76A9),
//                 fontSize: 26,
//                 fontFamily: 'poppins'),
//           ),
//               ],
//             ),
//           ),
//           Padding(
//             padding: EdgeInsets.only(left: 30),
//             child: Container(
//               height: 5,
//               width: screenWidth(context, dividedBy: 5),
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(10),
//                 color: Color(0xFF1B76A9),
//               ),
//             ),
//           ),
//           Container(
//             child: Row(
//               children: [
// Padding(
//   padding: EdgeInsets.only(
//       top: screenHeight(context, dividedBy: 20), left: 30),
//   child: Container(
//     height: 50,
//     width: 50,
//     decoration: BoxDecoration(
//       borderRadius: BorderRadius.circular(25),
//       color: Color(0xFFE3E4E8),
//     ),
// child: Padding(
//   padding: const EdgeInsets.all(8),
//   child: SvgPicture.asset(
//     "assets/images/location.svg",
//     height: 5,
//     width: 5,
//     color: Colors.black,
//   ),
// ),
//                   ),
//                 ),
//       Padding(
//         padding: EdgeInsets.only(
//             top: screenHeight(context, dividedBy: 20),
//             left: 10,
//             bottom: 1),
//         child: Container(
//           height: screenHeight(context, dividedBy: 11),
//           width: screenWidth(context, dividedBy: 1.7),
//           child: Column(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(top: 0, right: 100),
//                 child: Container(
//                   height: screenHeight(context, dividedBy: 25),
//                   width: screenWidth(context, dividedBy: 1),
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(25),
//                     color: Colors.white30,
//                   ),
//                   child: Padding(
//                     padding: const EdgeInsets.only(
//                         left: 1, top: 0, bottom: 0),
//   child: Text(
//     'Destinations',
//     style: TextStyle(
//         color: Color(0xFFABABAB),
//         fontSize: 18,
//         fontFamily: 'poppins'),
//   ),
// ),
//                 ),
//               ),
// Padding(
//   padding: EdgeInsets.only(
//       right: 50,
//       top: screenHeight(context, dividedBy: 100)),
//   child: Container(
//     height: 20,
//     width: 250,
//     child: TextFormField(
//       readOnly: true,
//       onTap: () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(
//               builder: (context) =>
//                   SearchDestination()),
//         );
//       },
//       decoration: InputDecoration(
// hintText: Hive.box('lang').get(10) != null
//     ? Hive.box('lang').get(10)
//     : "Select destination",
//         hintStyle: TextStyle(
//           fontSize: 15,
//           fontFamily: 'poppins',
//           color: Color(0xFF4D4D4D),
//           height: 0,
//         ),
//         border: InputBorder.none,
//       ),
//     ),
//   ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     ],
//   ),
// ),
//           Container(
//             child: Row(
//               children: [
//                 Padding(
//                   padding: EdgeInsets.only(
//                       top: screenHeight(context, dividedBy: 50), left: 30),
//                   child: Container(
//                     height: 50,
//                     width: 50,
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(25),
//                       color: Color(0xFFE3E4E8),
//                     ),
//                     child: Padding(
//                         padding: const EdgeInsets.all(8),
//                         child: Icon(Icons.calendar_today, size: 30)),
//                   ),
//                 ),
//                 Padding(
//                   padding: EdgeInsets.only(
//                       top: screenHeight(context, dividedBy: 50), left: 10),
//                   child: Container(
//                     height: screenHeight(context, dividedBy: 11),
//                     width: screenWidth(context, dividedBy: 1.7),
//                     color: Colors.white30,
//                     child: Column(
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.only(top: 0, right: 100),
//                           child: Container(
//                             height: screenHeight(context, dividedBy: 25),
//                             width: screenWidth(context, dividedBy: 1),
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(25),
//                               color: Colors.white30,
//                             ),
//                             child: Padding(
//                               padding: const EdgeInsets.only(
//                                   left: 1, top: 0, bottom: 0),
//                               child: Text(
//                                 'Check-In',
//                                 style: TextStyle(
//                                     color: Color(0xFFABABAB),
//                                     fontSize: 18,
//                                     fontFamily: 'poppins'),
//                               ),
//                             ),
//                           ),
//                         ),
//                         Padding(
//                           padding: EdgeInsets.only(
//                               right: 50,
//                               top: screenHeight(context, dividedBy: 100)),
//                           child: Container(
//                             height: 20,
//                             width: 250,
//                             child: TextFormField(
//                               readOnly: true,
//                               onTap: () {
//                                 Navigator.push(
//                                   context,
//                                   MaterialPageRoute(
//                                       builder: (context) => DatePicker()),
//                                 );
//                              },
//                               decoration: InputDecoration(
//                                 hintText: checkInDate != null
//                                     ? checkInDate
//                                     : "Select Date",
//                                 hintStyle: TextStyle(
//                                   fontSize: 15,
//                                   fontFamily: 'poppins',
//                                   color: Color(0xFF4D4D4D),
//                                   height: 0,
//                                 ),
//                                 border: InputBorder.none,
//                               ),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Container(
//             height: screenHeight(context, dividedBy: 17),
//             child: Column(children: [
//               Padding(
//                 padding: EdgeInsets.only(left: 50),
//                 child: Container(
//                   height: screenHeight(context, dividedBy: 125),
//                   width: screenWidth(context, dividedBy: 75),
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(25),
//                     color: Color(0xFFE3E4E8),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(
//                     top: screenHeight(context, dividedBy: 250), left: 50),
//                 child: Container(
//                   height: screenHeight(context, dividedBy: 125),
//                   width: screenWidth(context, dividedBy: 75),
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(25),
//                     color: Color(0xFFE3E4E8),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(
//                     top: screenHeight(context, dividedBy: 250), left: 50),
//                 child: Container(
//                   height: screenHeight(context, dividedBy: 125),
//                   width: screenWidth(context, dividedBy: 75),
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(25),
//                     color: Color(0xFFE3E4E8),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(
//                     top: screenHeight(context, dividedBy: 250), left: 50),
//                 child: Container(
//                   height: screenHeight(context, dividedBy: 125),
//                   width: screenWidth(context, dividedBy: 75),
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(25),
//                     color: Color(0xFFE3E4E8),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: EdgeInsets.only(
//                     top: screenHeight(context, dividedBy: 250), left: 50),
//                 child: Container(
//                   height: screenHeight(context, dividedBy: 125),
//                   width: screenWidth(context, dividedBy: 75),
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(25),
//                     color: Color(0xFFE3E4E8),
//                   ),
//                 ),
//               ),
//             ]),
//           ),
//           Container(
//             child: Row(
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.only(top: 0, left: 30),
//                   child: Container(
//                     height: 50,
//                     width: 50,
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(25),
//                       color: Color(0xFFE3E4E8),
//                     ),
//                     child: Padding(
//                         padding: const EdgeInsets.all(8),
//                         child: Icon(Icons.calendar_today, size: 30)),
//                   ),
//                 ),
//                 Padding(
//                   padding: EdgeInsets.only(
//                     left: 10,
//                   ),
//                   child: Container(
//                     height: screenHeight(context, dividedBy: 11),
//                     width: screenWidth(context, dividedBy: 1.7),
//                     color: Colors.white30,
//                     child: Column(
//                       children: [
//                         Padding(
//                           padding: EdgeInsets.only(
//                               top: screenHeight(context, dividedBy: 200),
//                               right: 150),
//                           child: Container(
//                             height: screenHeight(context, dividedBy: 25),
//                             width: screenWidth(context, dividedBy: 1),
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(25),
//                               color: Colors.white30,
//                             ),
//                             child: Padding(
//                               padding: const EdgeInsets.only(left: 1, top: 0),
//                               child: Text(
//                                 'Check-out',
//                                 style: TextStyle(
//                                     color: Color(0xFFABABAB),
//                                     fontSize: 18,
//                                     fontFamily: 'poppins'),
//                               ),
//                             ),
//                           ),
//                         ),
//                         Padding(
//                           padding: EdgeInsets.only(
//                               right: 50,
//                               top: screenHeight(context, dividedBy: 100)),
//                           child: Container(
//                             height: 20,
//                             width: 250,
//                             child: TextFormField(
//                               readOnly: true,
//                               onTap: () {
//                                 Navigator.push(
//                                   context,
//                                   MaterialPageRoute(
//                                       builder: (context) => DatePicker()),
//                                 );
//                               },
//                               decoration: InputDecoration(
//                                   hintText: checkOutDate != null
//                                       ? checkOutDate
//                                       : "Select Date",
//                                   hintStyle: TextStyle(
//                                     fontSize: 15,
//                                     fontFamily: 'poppins',
//                                     color: Color(0xFF4D4D4D),
//                                     height: 0,
//                                   ),
//                                   border: InputBorder.none),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Container(
//             child: Row(
//               children: [
//                 Padding(
//                   padding: EdgeInsets.only(
//                       top: screenHeight(context, dividedBy: 50), left: 30),
//                   child: Container(
//                     height: 50,
//                     width: 50,
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(25),
//                       color: Color(0xFFE3E4E8),
//                     ),
//                     child: Padding(
//                         padding: const EdgeInsets.all(8),
//                         child: Icon(Icons.calendar_today, size: 30)),
//                   ),
//                 ),
//                 Padding(
//                   padding: EdgeInsets.only(
//                     top: screenHeight(context, dividedBy: 50),
//                     left: 10,
//                   ),
//                   child: Container(
//                     height: screenHeight(context, dividedBy: 11),
//                     width: screenWidth(context, dividedBy: 1.7),
//                     child: Column(
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.only(top: 0, right: 100),
//                           child: Container(
//                             height: screenHeight(context, dividedBy: 30),
//                             width: screenWidth(context, dividedBy: 1),
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(25),
//                               color: Colors.white30,
//                             ),
//                             child: Padding(
//                               padding: const EdgeInsets.only(
//                                   left: 1, top: 0, bottom: 0),
//                               child: Text(
//                                 'Guests',
//                                 style: TextStyle(
//                                     color: Color(0xFFABABAB),
//                                     fontSize: 18,
//                                     fontFamily: 'poppins'),
//                               ),
//                             ),
//                           ),
//                         ),
//                         Padding(
//                           padding: EdgeInsets.only(
//                               right: 0,
//                               top: screenHeight(context, dividedBy: 100)),
//                           child: GestureDetector(
//                             onTap: () {
// Future(() => showModalBottomSheet(
//     shape: RoundedRectangleBorder(
//         borderRadius: BorderRadius.only(
//             topRight: Radius.circular(50),
//             topLeft: Radius.circular(50))),
//     context: context,
//     isScrollControlled: true,
//     builder: (context) {
//       return ModalBottomSheet();
//     }));
//                             },
//                             child: Container(
//                               height: 25,
//                               width: screenWidth(context, dividedBy: 1.7),
//                               child: Text(
//                                 guest == "no value"
//                                     ? "$rooms" +
//                                         ", " +
//                                         "$adults" +
//                                         ", " +
//                                         "$children"
//                                     : "Select guest",
//                                 style: TextStyle(
//                                   fontSize: 15,
//                                   fontFamily: 'poppins',
//                                   color: Color(0xFF4D4D4D),
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           Padding(
//             padding: EdgeInsets.only(
//                 top: screenHeight(context, dividedBy: 10),
//                 left: 30,
//                 bottom: screenHeight(context, dividedBy: 75)),
//             child: GestureDetector(
//               onTap: () {
//                 if (Hive.box('lang').get(10) != null) {
//                   Navigator.push(context,
//                       MaterialPageRoute(builder: (context) => Webview()));
//                 } else {
//                   final snackbar = SnackBar(
//                     content: Text(
//                       Constants.CHOOSE_DESTINATION,
//                       style: TextStyle(
//                           fontStyle: FontStyle.normal,
//                           fontFamily: 'Poppins',
//                           fontSize: 14.0),
//                     ),
//                     duration: Duration(seconds: 3),
//                     backgroundColor: Constants.kitGradients[0],
//                   );
//                   _globalKey.currentState.showSnackBar(snackbar);
//                 }
//               },
//               child: Container(
//                   height: 50,
//                   width: 300,
//                   decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(25),
//                       color: Color(
//                         0xFF1B76A9,
//                       )),
//                   child: Center(
//                       child: Text("Search",
//                           style: TextStyle(
//                               fontSize: 22,
//                               fontFamily: 'NostoSans',
//                               color: Colors.white)))),
//             ),
//           ),
//         ]),
//       ),
//     );
//   }
// }
