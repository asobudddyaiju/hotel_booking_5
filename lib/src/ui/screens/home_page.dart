import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/localization/locale.dart';
import 'package:hotel_booking_5/src/ui/screens/search_destination.dart';
import 'package:hotel_booking_5/src/ui/screens/webview.dart';
import 'package:hotel_booking_5/src/ui/widgets/calender.dart';
import 'package:hotel_booking_5/src/ui/widgets/guest_bottom_sheet.dart';
import 'package:hotel_booking_5/src/ui/widgets/home_drawer.dart';
import 'package:hotel_booking_5/src/ui/widgets/home_page_field_box.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/object_factory.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin{
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  var destination = "Select destination";
  String dates;
  String guest;
  String checkInDate;
  String checkOutDate;
  String nights;
  String rooms;
  String adults;
  String children;
  SharedPreferences _sharedPreferences;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseAuth _authfb = FirebaseAuth.instance;
  String monthInLetterCheckIn;
  String monthInLetterCheckOut;
  AnimationController animationController;
  Animation animationSize;

  void getSharedPref() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 4));
    animationSize = TweenSequence([
      TweenSequenceItem(tween: Tween(begin: 1.0, end: 2.0), weight: 1),
      TweenSequenceItem(tween: Tween(begin: 2.0, end: 1.0), weight: 1)
    ]).animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.8, 1.0)));
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    setState(() {
      monthInLetterCheckIn = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(5));
    });
    setState(() {
      monthInLetterCheckOut = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(Hive.box('code').get(6));
    });

    if (Hive.box('adult').get(5) != null) {
      setState(() {
        nights = Hive.box('adult').get(5).toString();
      });
    } else {
      nights = "No: of";
    }
    if (Hive.box('code').get(3) != null) {
      setState(() {
        checkInDate =
            Hive.box('code').get(3).toString() + " " + monthInLetterCheckIn;
      });
    } else {
      setState(() {
        checkInDate = "Checkin";
      });
    }
    if (Hive.box('code').get(6) != null) {
      setState(() {
        checkOutDate = "- " +
            Hive.box('code').get(4).toString() +
            " " +
            monthInLetterCheckOut;
      });
    } else {
      setState(() {
        checkOutDate = "Checkout";
      });
    }

    if (Hive.box('adult').get(1) != null) {
      setState(() {
        rooms = Hive.box('adult').get(1) + " Room";
      });
    } else {
      setState(() {
        rooms = "";
      });
    }
    if (Hive.box('adult').get(2) != null) {
      setState(() {
        adults = Hive.box('adult').get(2) + " Adult";
      });
    } else {
      setState(() {
        adults = "";
      });
    }
    if (Hive.box('adult').get(3) != null) {
      setState(() {
        children = Hive.box('adult').get(3) + " Children";
      });
    } else {
      setState(() {
        children = "";
      });
    }
    if ((Hive.box('adult').get(1) == null) &&
        (Hive.box('adult').get(3) == null) &&
        (Hive.box('adult').get(3) == null)) {
      setState(() {
        guest = "Select guest";
      });
    } else {
      setState(() {
        guest = "no value";
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _globalKey,
        backgroundColor: Colors.white,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0), // here the desired height
            child: AppBar(
              elevation: 0,
            )),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              child: Text(
                getTranslated(context, 'Search'),
                style: TextStyle(fontSize: 18,fontFamily: 'Gilroy',fontWeight: FontWeight.w300,fontStyle: FontStyle.normal),
              ),
              onTap: () {
                if (Hive.box('lang').get(10) != null) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Webview()));
                } else {
                  final snackbar = SnackBar(
                    content: Text(
                      Constants.CHOOSE_DESTINATION,
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontFamily: 'Poppins',
                          fontSize: 14.0),
                    ),
                    duration: Duration(seconds: 3),
                    backgroundColor: Constants.kitGradients[0],
                  );
                  _globalKey.currentState.showSnackBar(snackbar);
                }
              },
            ),Transform.scale(scale: animationSize.value,child:  MaterialButton(
              disabledColor: Constants.kitGradients[7],
              height: screenHeight(context, dividedBy: 8.8),
              shape: CircleBorder(),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
                size: 40,
              ),
              color: Constants.kitGradients[0],
              onPressed: () {
                if (Hive.box('lang').get(10) != null) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Webview()));
                } else {
                  final snackbar = SnackBar(
                    content: Text(
                      Constants.CHOOSE_DESTINATION,
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontFamily: 'Poppins',
                          fontSize: 14.0),
                    ),
                    duration: Duration(seconds: 2),
                    backgroundColor: Constants.kitGradients[0],
                  );
                  _globalKey.currentState.showSnackBar(snackbar);
                }
              },
            )),

          ],
        ),
        drawer: HomeDrawer(auth: _auth, authfb: _authfb),
        body: Builder(
          builder: (context) => SafeArea(
            top: true,
            left: true,
            bottom: true,
            child: Stack(
              children: [
                Column(
                  children: [
                    Container(
                      height: screenHeight(context, dividedBy: 2.4),
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/bg_image.png"),
                            fit: BoxFit.cover),
                      ),
                      child: Container(
                        height: screenHeight(context, dividedBy: 12),
                        width: screenWidth(context, dividedBy: 9),
                        child: Center(
                            child: Image.asset("assets/images/app_icon.png")),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Constants.kitGradients[7],
                                Constants.kitGradients[7].withOpacity(.95),
                                Constants.kitGradients[7].withOpacity(.90),
                                Constants.kitGradients[7].withOpacity(.80),
                                Constants.kitGradients[7].withOpacity(.70),
                                Constants.kitGradients[7].withOpacity(.50),
                                Constants.kitGradients[7].withOpacity(.40),
                                Constants.kitGradients[7].withOpacity(.30),
                                Constants.kitGradients[7].withOpacity(.20),
                                Constants.kitGradients[7].withOpacity(.10)
                              ]),
                        ),
                      ),
                    ),
                  ],
                ),
                Positioned(
                  left: 10,
                  top: 10,
                  child: GestureDetector(
                    child: Container(
                      child: Icon(
                        Icons.dehaze,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                  ),
                ),
                Positioned(
                  top: screenHeight(context, dividedBy: 2.8),
                  child: Container(
                    height: screenHeight(context, dividedBy: 2),
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.only(topLeft: Radius.circular(40)),
                        color: Colors.white),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 15),
                            ),
                            Text(
                              getTranslated(context, 'Search'),
                              style:
                                  TextStyle(fontSize: 24.0,color: Constants.kitGradients[2],fontWeight: FontWeight.w300,fontFamily: 'Gilroy',fontStyle: FontStyle.normal),
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 80),
                            ),
                            Text(
                              getTranslated(context, 'Hotels'),
                              style: TextStyle(
                                  fontSize: 24.0, fontWeight: FontWeight.w800,fontFamily: 'Gilroy',color: Constants.kitGradients[7],fontStyle: FontStyle.normal),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        // FutureBuilder<String>(
                        //     future: ObjectFactory().prefs.getDestination(),
                        //     builder: (context, snapshot) {
                        //       if (snapshot.hasData &&
                        //           snapshot.data.isNotEmpty) {
                        //         return HomePageFieldBox(
                        //           assetPath: "assets/images/location_icon.svg",
                        //           fieldVal: snapshot.data == "no value"
                        //               ? "Select"
                        //               : snapshot.data,
                        //           labelHead:
                        //               getTranslated(context, 'Destination'),
                        //           onPressed: () {
                        //             Navigator.push(
                        //               context,
                        //               MaterialPageRoute(
                        //                   builder: (context) =>
                        //                       SearchDestination()),
                        //             );
                        //           },
                        //         );
                        //       }
                        HomePageFieldBox(
                          assetPath: "assets/images/location3_icon.svg",
                          fieldVal: Hive.box('lang').get(10) != null
                              ? Hive.box('lang').get(10)
                              : "Select destination",
                          labelHead: getTranslated(context, 'Destination'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SearchDestination()),
                            );
                          },
                        ),
                        // }),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        HomePageFieldBox(
                          assetPath: "assets/images/calander.svg",
                          fieldVal: "$checkInDate" +
                              " " +
                              "$checkOutDate( $nights nights)",
                          labelHead: getTranslated(context, 'Dates'),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DatePicker()),
                            );
                          },
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 60),
                        ),
                        HomePageFieldBox(
                          assetPath: "assets/images/user.svg",
                          fieldVal: guest == "no value"
                              ? "$rooms" + ", " + "$adults" + ", " + "$children"
                              : "Select guest",
                          labelHead: getTranslated(context, 'Guests'),
                          onPressed: () {
                            // _guestModalBottomSheet(context);
                            Future(() => showModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(50),
                                        topLeft: Radius.circular(50))),
                                context: context,
                                isScrollControlled: true,
                                builder: (context) {
                                  return ModalBottomSheet();
                                }));
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
