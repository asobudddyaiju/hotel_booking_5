import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/localization/locale.dart';
import 'package:hotel_booking_5/src/auth_service/firebase_auth.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screens.dart';
import 'package:hotel_booking_5/src/ui/widgets/build_dropcurrency.dart';
import 'package:hotel_booking_5/src/ui/widgets/build_dropdown.dart';
import 'package:hotel_booking_5/src/ui/widgets/profile_list_tile.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

import 'package:hotel_booking_5/src/ui/screens/login.dart';

class ProfilePage extends StatefulWidget {
  FirebaseAuth auth;
  FirebaseAuth authfb;
  ProfilePage({this.auth, this.authfb});
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String buttonName = "Sign_in";
  String photpurl;
  String dummyphotourl =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png';
  String displayName;
  String dummydisplayName = 'Username';
  @override
  void initState() {
    if ((widget.auth.currentUser != null) ||
        (widget.authfb.currentUser != null)) {
      setState(() {
        buttonName = "Sign_Out";
        photpurl = Hive.box('lang').get(12);
        displayName = Hive.box('lang').get(13);
      });
    } else {
      setState(() {
        photpurl = dummyphotourl;
        displayName = dummydisplayName;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 20)),
          // here the desired height
          child: AppBar(
            leading: new IconButton(
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              },
            ),
            title: Text(
              'Profile',
              style: TextStyle(
                  color: Colors.black54, fontWeight: FontWeight.normal),
            ),
            elevation: 0,
            backgroundColor: Colors.white,
          )),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10, left: 10),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Text(
                    "Welcome to ",
                    style: TextStyle(
                        fontSize: 28,
                        color: Color(0xFF272727),
                        fontFamily: 'Gilroy'),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 100)),
                  child: Text(
                    " hotels!",
                    style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.w800,
                        color: Color(0xFF1B76A9),
                        fontFamily: 'Nexa'),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 10),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 1),
                    child: Text(
                      getTranslated(context, 'Region'),
                      style: TextStyle(fontSize: 14, color: Colors.black),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                        padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 50),
                        ),
                        child: Country()),
                  )
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 1),
                    child: Text(
                      getTranslated(context, 'Currency'),
                      style: TextStyle(fontSize: 14, color: Colors.black),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                        padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 10),
                        ),
                        child: DropCurrency()),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 20),
          ),
          Column(children: [
            Container(
              height: screenHeight(context, dividedBy: 12),
              child: ProfileListTile(
                listText: 'About us',
                listText1: '',
              ),
            ),
            Container(
              height: screenHeight(context, dividedBy: 12),
              child: ProfileListTile(
                listText: 'Help and feedback',
                listText1: '',
              ),
            ),
            Container(
              height: screenHeight(context, dividedBy: 12),
              child: ProfileListTile(
                listText: 'Term and conditions',
                listText1: '',
              ),
            ),
            Container(
              height: screenHeight(context, dividedBy: 12),
              child: ProfileListTile(
                listText: 'Privacy policy',
                listText1: '',
              ),
            ),
            GestureDetector(
              onTap: () {
                if ((widget.auth.currentUser == null) &&
                    (widget.authfb.currentUser == null)) {
                  // Navigator.pushAndRemoveUntil(
                  //  context,
                  //  MaterialPageRoute(
                  //      builder: (context) => Login(auth: widget.auth)),
                  //  (route) => false);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              Login(auth: widget.auth, authFb: widget.authfb)));
                } else {
                  void signout() async {
                    await Auth(auth: widget.auth, authfb: widget.authfb)
                        .Signout();
                  }

                  signout();

                  setState(() {
                    buttonName = "Sign in";
                    photpurl = dummyphotourl;
                    displayName = dummydisplayName;
                  });
                }
              },
              child: Container(
                height: screenHeight(context, dividedBy: 12),
                child: ProfileListTile(
                  listText: buttonName,
                  listText1: '',
                ),
              ),
            ),
          ]),
        ],
      ),
    );
  }
}
