import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Webview extends StatefulWidget {
  @override
  _WebviewState createState() => _WebviewState();
}

//String url =
//    'http://book.hotelmost/SearchTermTypeRedirection.ashx?destination=$destination&useStored=$userStored&checkin=$checkin&checkout=$checkout&adults';
//String url_2 =
//    '=$ages&Rooms=$rooms&languageCode=$languageCode&currencyCode=$currencyCode';
//String web;
//void increment() {
// for (var i = 1; i <= adults; i++) {
//  adultx = i.toString();
//}
//for (var j = 1; j <= adults; j++) childage = j.toString();
//}
String currentUrl;
final Completer<WebViewController> _controller = Completer<WebViewController>();
List<String> age;
String url = '';
String dest = '';
String destination = dest.replaceAll(' ', '_');
String userStored = "false";
String checkin = '';
String checkout = '';
String x = '';
String baseUrl =
    'http://book.hotelmost.com/SearchTermTypeRedirection.ashx?destination=Place%3A';
String adult = '';
String child = '';
String ageChild = '';
String ageChild1 = '';
String adults = '';
String adult1 = '';
String childAge = '';
String ages = '';
String child1 = '';
String child2 = '';
String child3 = '';
String room = '';
String rooms = '';
String langCode = '';
String currencyCode = '';
String date = '';

class _WebviewState extends State<Webview> {
  // final webviewPlugin = new FlutterWebviewPlugin();
  // GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  // StreamSubscription<String> _onUrlChanged;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    setState(() {
      adults = Hive.box('adult').get(2);
      rooms = Hive.box('adult').get(1);
      x = Hive.box('adult').get(1);
    });
    checkin = Hive.box('lang').get(5) as String;
    checkout = Hive.box('lang').get(6) as String;
    dest = Hive.box('lang').get(10);
    age = Hive.box('adult').get(4);
    child = Hive.box('adult').get(3);
    langCode = Hive.box('code').get(1) != null ? Hive.box('code').get(1) : 'EN';
    currencyCode =
    Hive.box('code').get(2) != null ? Hive.box('code').get(2) : 'USD';
    if (age == null) {
      print('error on child');
    } else {
      if (age.length != null) {
        if (age.length != 1) {
          if (age.length != 2) {
            if (age.length != 3) {} else {
              child1 = age[0].replaceAll('Years', '');
              child2 = age[1].replaceAll('Years', '');
              child3 = age[2].replaceAll('Years', '');
              print('3 val');
            }
          } else {
            child1 = age[0].replaceAll('Years', '');
            child2 = age[1].replaceAll('Years', '');
            print('2 val');
          }
        } else {
          child1 = age[0].replaceAll('Years', '');
          print('1 val');
        }
      } else {}
    }
    setState(() {
      if (child == null) {
        print('error on age');
      }
      if (child == '0') {
        ageChild = '';
        ageChild1 = '';
      } else {
        if (child == '1') {
          ages = child1;
          ageChild = '&childAges_$x=';
          ageChild1 = ages;
        } else {
          if (child == '2') {
            ages = child1 + ',' + child2;
            ageChild = '&childAges_$x=';
            ageChild1 = ages;
          } else {
            if (child == '3') {
              ages = child1 + ',' + child2 + ',' + child3;
              ageChild = '&childAges_$x=';
              ageChild1 = ages;
            }
          }
        }
      }

      if ((adults == '0') || (adults == null)) {
        adult = '';
      } else {
        adult = '&adults_1=' + adults;
      }
      if (checkin == null) {
        date = '';
      } else {
        date = '&checkin=' + checkin + '&checkout=' + checkout;
      }
      if ((rooms == null) || (rooms == '0')) {
        room = '';
      } else {
        room = '&Rooms=' + rooms;
      }
    });
    // if (child == '1') {
    //   ages = child1;
    // }
    // if (child == '2') {
    //   ages = child1 + child2;
    // }
    // if (child == '3') {
    //   ages = '$child1$child2$child3';
    // }

    print(adults);
    print(destination);
    print(ages);
    print(langCode);
    url = baseUrl +
        destination +
        '&useStored=' +
        userStored +
        date +
        adult +
        ageChild +
        ageChild1 +
        room +
        '&languageCode=' +
        langCode +
        '&currencyCode=' +
        currencyCode;
    print(url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        WebView(
          onPageFinished: (finished) {
            setState(() {
              loading = false;
            });
          },

          onWebResourceError: (WebResourceError webview) {
            showAlert(context, 'Check your connection');
          },
          initialUrl: url,
          // 'http://book.hotelmost.com/SearchTermTypeRedirection.ashx?destination=$destination&useStored=$userStored&checkin=$checkin&checkout=$checkout&adults_1=$adults&childAges_1=$ages&Rooms=$rooms&languageCode=$languageCode&currencyCode=$currencyCode',

          javascriptMode: JavascriptMode.unrestricted,
        ),
        loading == true
            ? Container(
                height: screenHeight(context, dividedBy: 1),
                width: screenWidth(context, dividedBy: 1),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Expanded(
                        child: Shimmer.fromColors(
                          baseColor: Colors.grey[300],
                          highlightColor: Colors.grey[100],
                          enabled: loading,
                          child: ListView.builder(
                            itemBuilder: (_, __) => Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: screenWidth(context, dividedBy: 6),
                                    height: screenHeight(context, dividedBy: 9),
                                    color: Colors.white,
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8.0),
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          width: double.infinity,
                                          height: 8.0,
                                          color: Colors.white,
                                        ),
                                        const Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 2.0),
                                        ),
                                        Container(
                                          width: double.infinity,
                                          height: 8.0,
                                          color: Colors.white,
                                        ),
                                        const Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 2.0),
                                        ),
                                        Container(
                                          width: 40.0,
                                          height: 8.0,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            itemCount: 8,
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
            : Stack()
      ],
    ));
  }
}

