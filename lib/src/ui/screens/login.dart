import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hotel_booking_5/src/auth_service/firebase_auth.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screens.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/object_factory.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class Login extends StatefulWidget {
  final FirebaseAuth auth, authFb;

  const Login({this.auth, this.authFb});

  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends State<Login> {
  bool _isLoading;
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(statusBarColor: Constants.kitGradients[0]),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(screenHeight(context, dividedBy: 15)),
            // here the desired height
            child: AppBar(
              leading: new IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: new Icon(
                  Icons.close,
                  color: Colors.black,
                ),
              ),
              backgroundColor: Colors.white,
              elevation: 0.5,
            )),
        key: _globalKey,
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Center(
            child: Stack(
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 3),
                    ),
                    Center(
                      child: Text(
                        'Sign in and save',
                        style: TextStyle(
                            fontFamily: 'NexaBold',
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF484848),
                            fontSize: 18.0,
                            fontStyle: FontStyle.normal),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 10.0, bottom: 10.0, left: 20.0, right: 20.0),
                      child: Center(
                        child: Text(
                          'Track prices, organize travel plans and access member-only with your HotelBooking account',
                          style: TextStyle(
                              fontFamily: 'NexaLight',
                              fontWeight: FontWeight.w300,
                              color: Color(0xFF7E7E7E),
                              fontSize: 10.0,
                              fontStyle: FontStyle.normal),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(left: 35.0, right: 35.0, top: 20.0),
                      child: Center(
                        child: GestureDetector(
                          onTap: () async {
                            googleSignin();
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60.0,
                            decoration: BoxDecoration(
                                border: Border.all(style: BorderStyle.none),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFDBE7EB).withOpacity(0.2),
                                      offset: Offset(0, 5),
                                      blurRadius: 1.0,
                                      spreadRadius: 1.0)
                                ]),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/google_icon.png',
                                      width: 20.0, height: 20.0),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      'Continue with Google',
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300,
                                          fontStyle: FontStyle.normal,
                                          color: Color(0xFF484848)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(left: 35.0, right: 35.0, top: 20.0),
                      child: Center(
                        child: GestureDetector(
                          onTap: () async {
                            facebookSignin();
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60.0,
                            decoration: BoxDecoration(
                                border: Border.all(style: BorderStyle.none),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFDBE7EB).withOpacity(0.2),
                                      offset: Offset(0, 5),
                                      spreadRadius: 1.0,
                                      blurRadius: 1.0)
                                ]),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/facebook_icon.png',
                                      width: 20.0, height: 20.0),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      'Continue with Facebook',
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300,
                                          fontStyle: FontStyle.normal,
                                          color: Color(0xFF484848)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SafeArea(
                      child: Center(
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: EdgeInsets.only(
                                top: screenHeight(context, dividedBy: 6.5),
                                bottom: 0.0,
                                left: 15.0,
                                right: 15.0),
                            child: RichText(
                              text: TextSpan(
                                  text: 'By signing up you agree to our',
                                  style: TextStyle(
                                    fontFamily: 'NexaLight',
                                    fontWeight: FontWeight.w300,
                                    color: Color(0xFF404040),
                                    fontSize: 10.0,
                                  ),
                                  children: <TextSpan>[
                                    TextSpan(
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () => print(
                                              'you just clicked terms and conditions'),
                                        text: ' Terms & Conditions',
                                        style: TextStyle(
                                            fontFamily: 'NexaLight',
                                            fontWeight: FontWeight.w300,
                                            color: Colors.blue,
                                            fontSize: 10.0)),
                                    TextSpan(
                                        text: ' and',
                                        style: TextStyle(
                                            fontFamily: 'NexaLight',
                                            fontWeight: FontWeight.w300,
                                            fontSize: 10.0,
                                            color: Color(0xFF404040))),
                                    TextSpan(
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () => print(
                                              'you just clicked Privacy Policy'),
                                        text: ' Privacy Policy',
                                        style: TextStyle(
                                            fontFamily: 'NexaLight',
                                            fontWeight: FontWeight.w300,
                                            color: Colors.blue,
                                            fontSize: 10.0))
                                  ]),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                _isLoading == true
                    ? Center(
                        child: Container(
                            height: 40.0,
                            width: 40.0,
                            child: Center(child: CircularProgressIndicator())),
                      )
                    : Stack()
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showSnackbar(String snackdata) {
    String snackbuttonlabel, snackbarmessage;
    if (snackdata.trim() ==
        'An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.') {
      snackbuttonlabel = 'Continue with existing account';
      snackbarmessage =
          'An account already exists with the same email created with google sign in ';
    } else {
      snackbuttonlabel = 'OK';
    }
    final snack = SnackBar(
      content: Text(
        snackbarmessage,
        style: TextStyle(
            fontFamily: "Poppins", fontStyle: FontStyle.normal, fontSize: 12.0),
      ),
      duration: Duration(seconds: 15),
      action: SnackBarAction(
        label: snackbuttonlabel,
        onPressed: () {
          LinkGoogleAuthAndFacebookAuth();
        },
        textColor: Constants.kitGradients[10],
      ),
      backgroundColor: Constants.kitGradients[0],
    );
    _globalKey.currentState.showSnackBar(snack);
  }

  void googleSignin() async {
    setState(() {
      _isLoading = true;
    });
    String googlesigninstatus =
        await Auth(auth: widget.auth, authfb: widget.authFb).signInWithGoogle();
    if (googlesigninstatus == "Success") {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
          (route) => false);
    } else {
      setState(() {
        _isLoading = false;
      });
      _showSnackbar(googlesigninstatus.trim());
    }
  }

  void facebookSignin() async {
    setState(() {
      _isLoading = true;
    });
    String status = await Auth(auth: widget.auth, authfb: widget.authFb)
        .signInWithFacebook();
    if (status == "Success") {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
          (route) => false);
    } else {
      _showSnackbar(status.trim());
      print(status);
      setState(() {
        _isLoading = false;
      });
    }
  }

  void LinkGoogleAuthAndFacebookAuth() async {
    setState(() {
      _isLoading = true;
    });
    String status = await Auth(auth: widget.auth, authfb: widget.authFb)
        .linkGoogleAndFacebook();
    if (status == "Success") {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
          (route) => false);
    } else {
      _showSnackbar(status.trim());
      print(status);
      setState(() {
        _isLoading = false;
      });
    }
  }
}
