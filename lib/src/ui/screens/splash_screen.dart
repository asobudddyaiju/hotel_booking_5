import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screens.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Timer _timerControl;

  void startTimer() {
    _timerControl = Timer.periodic(const Duration(seconds: 2), (timer) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
          (route) => false);
      _timerControl.cancel();
    });
  }

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Align(
        alignment: Alignment.center,
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 3),
            ),
            Text(
              "hotels",
              style: TextStyle(
                color: Constants.kitGradients[13],
                fontSize: 28,
                fontFamily: 'JosefinSans',
                fontWeight: FontWeight.w600,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 9),
              child: Text(
                "Version 125.5",
                style: TextStyle(
                    color: Constants.kitGradients[2],
                    fontSize: 6,
                    fontWeight: FontWeight.w300,
                    fontFamily: 'Poppins'),
              ),
            ),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      top: 2, left: screenWidth(context, dividedBy: 2.4)),
                  child: Text(
                    " \u00a9 2020 hotello.com",
                    style: TextStyle(
                        color: Constants.kitGradients[13],
                        fontSize: 6,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: screenHeight(context, dividedBy: 20)),
              child: Text(
                "The best hotel deals",
                style: TextStyle(
                    color: Constants.kitGradients[14],
                    fontSize: 14,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: screenHeight(context, dividedBy: 150)),
              child: Text(
                "Compare the prices from all leading travel \n agencies and hotel websites.booking.com,\n"
                "Expedia, Agoda and many more- in one app.",
                style: TextStyle(
                    color: Constants.kitGradients[13],
                    fontFamily: 'Poppins',
                    fontSize: 12),
                overflow: TextOverflow.clip,
              ),
            )
          ],
        ),
      ),
    );
  }
}
