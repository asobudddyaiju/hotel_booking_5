import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/src/ui/screens/search_destination.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class Field extends StatefulWidget {
  String label;
  String data;
  String icon;
  Function onPressed;

  Field({this.label, this.data, this.icon, this.onPressed});

  @override
  _FieldState createState() => _FieldState();
}

class _FieldState extends State<Field> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: screenHeight(context, dividedBy: 12),
        width: screenWidth(context, dividedBy: 1),
        child: Row(
          children: [
            Padding(
                padding: EdgeInsets.only(top: 0),
                child: Container(
                  height: screenHeight(context, dividedBy: 15),
                  width: screenWidth(context, dividedBy: 8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        bottomRight: Radius.circular(30)),
                    color: Constants.kitGradients[12],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Container(
                      height: screenHeight(context, dividedBy: 50),
                      width: screenWidth(context, dividedBy: 50),
                      child: SvgPicture.asset(
                        widget.icon,
                        height: screenHeight(context, dividedBy: 50),
                        width: screenWidth(context, dividedBy: 50),
                        color: Colors.white,
                      ),
                    ),
                  ),
                )),
            SizedBox(
              height: screenHeight(context, dividedBy: 15),
              width: screenWidth(context, dividedBy: 10),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.all(0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      height: screenHeight(context, dividedBy: 25),
                      width: screenWidth(context, dividedBy: 4),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Text(
                        widget.label,
                        style: TextStyle(
                          fontSize: 14,
                          fontFamily: 'poppins',
                          color: Constants.kitGradients[4],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 200)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 30),
                    width: screenWidth(context, dividedBy: 2),
                    child: TextFormField(
                      readOnly: true,
                      onTap: widget.onPressed,
                      decoration: InputDecoration(
                        hintText: widget.data,
                        hintStyle: TextStyle(
                          fontSize: 18,
                          fontFamily: 'poppins',
                          color: Color(0xFF4D4D4D),
                          fontWeight: FontWeight.w500,
                          height: 0,
                        ),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ));
  }
}
