import 'dart:convert' as convert;

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/src/bloc/user_bloc.dart';
import 'package:hotel_booking_5/src/models/destination_list_response.dart';
import 'package:hotel_booking_5/src/utils/object_factory.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class SearchDestination extends StatefulWidget {
  @override
  _SearchDestinationState createState() => _SearchDestinationState();
}

class _SearchDestinationState extends State<SearchDestination> {
  TextEditingController destinationTextEditingController =
      new TextEditingController();
  List<String> destinationSuggestionList = [];
  bool isLoading = false;

  void getDestination() {
    // userBlocSingle.destinationList(searchKey: destinationTextEditingController.text);
    setState(() {
      isLoading = true;
    });
    final response = ObjectFactory()
        .apiClient
        .destinationList(searchKey: destinationTextEditingController.text);
    response.then((value) {
      setState(() {
        isLoading = false;
      });
      var jsonResponse = convert.jsonDecode(value.data);
      print(jsonResponse);
      List<String>.from(jsonResponse.map((x) {
        print(x["n"]);
        setState(() {
          destinationSuggestionList.add(x["n"]);
        });
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 20)),
          // here the desired height
          child: AppBar(
            leading: new IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
            ),
            backgroundColor: Colors.white,
            elevation: 0.5,
          )),
      body: StreamBuilder<DestinationListResponse>(
          stream: userBlocSingle.destinationResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) print(snapshot.data);
            return Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Container(
                    height: screenHeight(context, dividedBy: 14),
                    child: SimpleAutoCompleteTextField(
                      suggestionsAmount: 10,
                      submitOnSuggestionTap: true,
                      textChanged: (_) {
                        print(destinationTextEditingController.text);
                        if (destinationTextEditingController.text.trim() != "")
                          Hive.box('lang').get(10);
                      },
                      textSubmitted: (text) {
                        print(text);
                        Hive.box('lang').put(10, text);
                        Navigator.pop(context);
                      },
                      clearOnSubmit: false,
                      controller: destinationTextEditingController,
                      suggestions: destinationSuggestionList,
                      decoration: InputDecoration(
                        suffixIcon: GestureDetector(
                          child: Icon(Icons.search),
                          onTap: () {
                            // userBlocSingle.destinationList(searchKey: destinationTextEditingController.text);
                            setState(() {
                              isLoading = true;
                            });
                            final response = ObjectFactory()
                                .apiClient
                                .destinationList(
                                    searchKey:
                                        destinationTextEditingController.text);
                            response.then((value) {
                              setState(() {
                                isLoading = false;
                              });
                              var jsonResponse = convert.jsonDecode(value.data);
                              print(jsonResponse);
                              List<String>.from(jsonResponse.map((x) {
                                print(x["n"]);
                                setState(() {
                                  destinationSuggestionList.add(x["n"]);
                                  // destinationTextEditingController.text =
                                  //     x["n"];
                                });
                              }));
                            });
                          },
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(150),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        contentPadding: EdgeInsets.all(16),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  isLoading == true ? CircularProgressIndicator() : Container(),
                ],
              ),
            );
          }),
    );
  }
}
