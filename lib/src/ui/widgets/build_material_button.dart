import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/src/ui/screens/home_page.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screens.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class BuildMaterialButton extends StatefulWidget {
  int roomCount;
  int adultCount;
  int childrenCount;
  List<String> ChildrenAgeList=[];
  BuildMaterialButton({this.roomCount,this.adultCount,this.childrenCount,this.ChildrenAgeList});


  @override
  _BuildMaterialButtonState createState() => _BuildMaterialButtonState();
}

class _BuildMaterialButtonState extends State<BuildMaterialButton> {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Constants.kitGradients[0],
      minWidth: screenWidth(context, dividedBy: 1.2),
      height: 50,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      child: Text(
        'Done',
        style: TextStyle(color: Colors.white, fontSize: 22),
      ),
      onPressed: () {
        Hive.box('adult').put(1,widget.roomCount.toString());
        Hive.box('adult').put(2, widget.adultCount.toString());
        Hive.box('adult').put(3, widget.childrenCount.toString());
        // Hive.box('adult').put(4, widget.ChildrenAgeList);
        print("Rooms = "+Hive.box('adult').get(1).toString());
        print("Adults = "+Hive.box('adult').get(2).toString());
        print("Children = "+Hive.box('adult').get(3).toString());
        print("Children Age List = ");
        print(Hive.box('adult').get(4));
        Navigator.push(context, MaterialPageRoute(builder: (context)=>HomeScreen()));
      },
    );
  }
}
