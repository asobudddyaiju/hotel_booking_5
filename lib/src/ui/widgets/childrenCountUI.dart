import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/object_factory.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class ChildrenCountUI extends StatefulWidget {
  final int childrenCount;
  final ValueChanged<String> getage;
  String hintText;

  ChildrenCountUI({this.childrenCount,this.getage,this.hintText});


  @override
  _ChildrenCountUIState createState() => _ChildrenCountUIState();

}

class _ChildrenCountUIState extends State<ChildrenCountUI> {
  @override
  void initState() {
    if(widget.hintText==null){
      widget.hintText="Age";
    }
    super.initState();
  }
  String dropdownvalue;
  List<String> childrenAgeList = [
    "1 Years",
    "2 Years",
    "3 Years",
    "4 Years",
    "5 Years",
    "6 Years",
    "7 Years",
    "8 Years",
    "9 Years",
    "10 Years",
    "11 Years",
    "12 Years",
    "13 Years",
    "14 Years",
    "15 Years",
    "16 Years",
    "17 Years",
  ];

  @override
  Widget build(BuildContext context) {
    String hintFinal;
    if(widget.hintText!=null){hintFinal=widget.hintText;}else{hintFinal="Age";};
    return Container(
      child: Row(
        children: <Widget>[
          SizedBox(
            width: screenWidth(context, dividedBy: 20),
          ),
          Container(
            width: screenWidth(context, dividedBy: 5),
            child: new Text(
              "Child " +
                  (widget.childrenCount > 0
                      ? widget.childrenCount.toString()
                      : ""),
              style: TextStyle(
                  fontSize: 16,
                  color: Constants.kitGradients[2],
                  fontWeight: FontWeight.w300,fontStyle: FontStyle.normal,fontFamily: 'Poppins'
              ),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 2.8),
          ),
          Container(
            // width: 100,
              height: 60,
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  items: childrenAgeList
                      .map<DropdownMenuItem<String>>((String value) =>
                      DropdownMenuItem<String>(
                          value: value, child: Text(value)))
                      .toList(),
                  hint: Text(
                    hintFinal.toString(),
                    style: TextStyle(
                        color: Constants.kitGradients[0],
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                        fontFamily: 'Poppins'),
                  ),
                  iconSize: 0.0,
                  isExpanded: false,
                  value: dropdownvalue,
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Poppins'),
                  onChanged: (value) {
                    widget.getage(value);
                    setState(() {
                      dropdownvalue = value;
                    });
                  },
                ),
              )),
        ],
      ),
    );
  }
}
