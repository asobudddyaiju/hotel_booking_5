import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class HomePageFieldBox extends StatefulWidget {
  final String assetPath;
  final String labelHead;
  final String fieldVal;
  final Function onPressed;

  HomePageFieldBox(
      {this.assetPath, this.fieldVal, this.labelHead, this.onPressed});

  @override
  _HomePageFieldBoxState createState() => _HomePageFieldBoxState();
}

class _HomePageFieldBoxState extends State<HomePageFieldBox> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
            height: screenHeight(context, dividedBy: 9),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
              color: Colors.transparent,
            ),
            child: GestureDetector(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 8),
                child: Card(
                  // elevation: 300,
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: Container(
                      height: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.transparent,
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(top: 5,bottom:5),
                        child: Center(child: Row(
                        children: [
                          SizedBox(
                            width: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: SvgPicture.asset(widget.assetPath,color: Constants.kitGradients[10],),
                          ),
                          SizedBox(
                            width: 2,
                          ),
                          Text(
                            widget.fieldVal,
                            style: TextStyle(color: Constants.kitGradients[2], fontSize: 15,fontWeight: FontWeight.w300,fontStyle: FontStyle.normal,fontFamily: 'Gilroy'),
                          )
                        ],
                      ),),),
                    ),
                  ),
                ),
              ),
              onTap: () {
                widget.onPressed();
              },
            )),
        Positioned(
          left: 80,
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Container(
              height: 20,
              // width: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Constants.kitGradients[7],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
                child: Center(child: Text(
                  widget.labelHead,
                  style:
                  TextStyle(color: Constants.kitGradients[1], fontSize: 10,fontWeight: FontWeight.w300,fontStyle: FontStyle.normal,fontFamily: 'Gilroy'),
                ),),
              ),
            ),
          ),
        )
      ],
    );
  }
}
