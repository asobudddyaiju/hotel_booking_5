import 'package:flutter/material.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/src/ui/widgets/build_material_button.dart';
import 'package:hotel_booking_5/src/ui/widgets/childrenCountUI.dart';
import 'package:hotel_booking_5/src/ui/widgets/room_person_count.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class ModalBottomSheet extends StatefulWidget {
  _ModalBottomSheetState createState() => _ModalBottomSheetState();
}

class _ModalBottomSheetState extends State<ModalBottomSheet>
    with SingleTickerProviderStateMixin {
  var heightOfModalBottomSheet = 100.0;
  int counterValueRoom = 1;
  int counterValueAdults = 1;
  int counterValueChildren = 0;
  bool showLimit = false;
  List<String> agelist = [];
  String ageValue;
  String hint1, hint2, hint3;

  @override
  void initState() {
    if (Hive.box('adult').get(1) != null) {
      setState(() {
        counterValueRoom = int.parse(Hive.box('adult').get(1));
      });
    } else {
      setState(() {
        counterValueRoom = 1;
      });
    }
    if (Hive.box('adult').get(2) != null) {
      setState(() {
        counterValueAdults = int.parse(Hive.box('adult').get(2));
      });
    } else {
      setState(() {
        counterValueAdults = 1;
      });
    }
    if (Hive.box('adult').get(3) != null) {
      setState(() {
        counterValueChildren = int.parse(Hive.box('adult').get(3));
      });
    } else {
      setState(() {
        counterValueChildren = 0;
      });
    }
    if (Hive.box('adult').get(4) != null) {
      agelist = Hive.box('adult').get(4);
      if (agelist.length == 1) {
        setState(() {
          hint1 = agelist[0];
        });
      }
      if (agelist.length == 2) {
        setState(() {
          hint1 = agelist[0];
          hint2 = agelist[1];
        });
      }
      if (agelist.length == 3) {
        setState(() {
          hint1 = agelist[0];
          hint2 = agelist[1];
          hint3 = agelist[2];
        });
      }
    } else {
      setState(() {
        agelist = [];
      });
    }

    super.initState();
  }

  List<String> years = [
    "1 Years",
    "2 Years",
    "3 Years",
    "4 Years",
    "5 Years",
    "6 Years",
    "7 Years",
    "8 Years",
    "9 Years",
    "10 Years",
    "11 Years",
    "12 Years",
    "13 Years",
    "14 Years",
    "15 Years",
    "16 Years",
    "17 Years",
  ];

  Widget build(BuildContext context) {
    return Container(
      // height: screenHeight(context, dividedBy: 2),
      child: new Wrap(
        children: <Widget>[
          Row(
            children: [
              Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                child: Text(
                  "Guest",
                  style: TextStyle(
                      fontFamily: 'Nexa',
                      fontSize: 18,
                      fontWeight: FontWeight.w700),
                ),
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 2),
              ),
              GestureDetector(
                child: Text("Cancel",
                    style: TextStyle(color: Colors.grey, fontSize: 11)),
                onTap: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
          showLimit == true
              ? Center(
            child: Container(
              height: screenHeight(context, dividedBy: 30),
              width: screenWidth(context, dividedBy: 1.2),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image(
                      image: AssetImage(Constants.CAUTION_SIGN),
                    ),
                    Text(
                      Constants.GUEST_LIMIT_ERROR,
                      style: TextStyle(
                          color: Constants.kitGradients[5],
                          fontSize: 9.0,
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Poppins',
                          fontStyle: FontStyle.normal),
                    ),
                  ],
                ),
              ),
            ),
          )
              : Container(
            height: screenHeight(context, dividedBy: 40),
            width: screenWidth(context, dividedBy: 1.2),
          ),
          RoomPersonCount(
              counterLabel: "Room",
              counterValue: counterValueRoom,
              decFunction: () {
                if (counterValueRoom > 1) {
                  setState(() => counterValueRoom--);
                }
                if (counterValueRoom <= 9) {
                  setState(() {
                    showLimit = false;
                  });
                }
              },
              incFunction: () {
                if (counterValueRoom < 9) {
                  setState(() => counterValueRoom++);
                } else {
                  setState(() {
                    showLimit = true;
                  });
                }
              }),
          Center(
            child: Container(
              height: 1,
              width: screenWidth(context, dividedBy: 1.1),
              color: Constants.kitGradients[2].withOpacity(.06),
            ),
          ),

          RoomPersonCount(
            counterLabel: "Adult",
            counterValue: counterValueAdults,
            decFunction: () {
              if (counterValueAdults > 1) {
                setState(() => counterValueAdults--);
              }
              if ((counterValueAdults + counterValueChildren) <= 16) {
                setState(() {
                  showLimit = false;
                });
              }
            },
            incFunction: () {
              if (counterValueAdults < counterValueRoom * 3 &&
                  counterValueRoom < 9) {
                setState(() => counterValueAdults++);
              } else if (counterValueRoom < 9) {
                setState(() => counterValueRoom++);
                setState(() => counterValueAdults++);
              }
              if ((counterValueAdults + counterValueChildren) > 16) {
                setState(() {
                  showLimit = true;
                });
              }
            },
          ),
          Center(
            child: Container(
              height: 1,
              width: screenWidth(context, dividedBy: 1.1),
              color: Constants.kitGradients[2].withOpacity(.06),
            ),
          ),

          RoomPersonCount(
            counterLabel: "Children",
            counterValue: counterValueChildren,
            incFunction: () {
              if (counterValueAdults > 0 &&
                  ((counterValueAdults + counterValueChildren) <
                      counterValueRoom * 3) &&
                  counterValueChildren < 4) {
                setState(() => counterValueChildren++);
              }
              if ((counterValueAdults + counterValueChildren) > 16) {
                setState(() {
                  showLimit = true;
                });
              }
            },
            decFunction: () {
              if (counterValueChildren > 0) {
                setState(() => counterValueChildren--);
                if (counterValueChildren == 0) {
                  setState(() {
                    agelist.removeAt(0);
                  });
                }
                if (counterValueChildren == 1) {
                  setState(() {
                    agelist.removeAt(1);
                    agelist.removeAt(2);
                  });
                }
              }
              if (counterValueChildren == 2) {
                setState(() {
                  agelist.removeAt(2);
                });
              }

              if ((counterValueAdults + counterValueChildren) <= 16) {
                setState(() {
                  showLimit = false;
                });
              }
            },
          ),
          counterValueChildren > 0
              ? Container(
            height: screenHeight(context, dividedBy: 30),
            width: screenWidth(context, dividedBy: 1.2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 19.5),
                ),
                Text(
                  Constants.AGE_OF_CHILD,
                  style: TextStyle(
                      color: Constants.kitGradients[2],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Poppins',
                      fontStyle: FontStyle.normal),
                ),
              ],
            ),
          )
              : Container(
            height: screenHeight(context, dividedBy: 40),
            width: screenWidth(context, dividedBy: 1.2),
          ),
          counterValueChildren > 0
              ? ChildrenCountUI(
            hintText: hint1,
            childrenCount: 1,
            getage: (val) {
              ageValue = val;
              if (agelist.isNotEmpty) {
                setState(() {
                  agelist[0] = ageValue;
                });

                Hive.box('adult').put(4, agelist);
              } else {
                agelist.add(ageValue);
                Hive.box('adult').put(4, agelist);
              }
            },
          )
              : Container(
            height: 0,
            width: 0,
          ),
          counterValueChildren > 1
              ? ChildrenCountUI(
            hintText: hint2,
            childrenCount: 2,
            getage: (val2) {
              ageValue = val2;
              if (agelist.length == 1) {
                agelist.add(ageValue);
                Hive.box('adult').put(4, agelist);
              } else {
                setState(() {
                  agelist[1] = ageValue;
                });
                Hive.box('adult').put(4, agelist);
              }
            },
          )
              : Container(
            height: 0,
            width: 0,
          ),
          counterValueChildren > 2
              ? ChildrenCountUI(
            hintText: hint3,
            childrenCount: 3,
            getage: (val2) {
              ageValue = val2;
              if (agelist.length == 2) {
                agelist.add(ageValue);
                Hive.box('adult').put(4, agelist);
              } else {
                setState(() {
                  agelist[2] = ageValue;
                });
                Hive.box('adult').put(4, agelist);
              }
            },
          )
              : Container(
            height: 0,
            width: 0,
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
          Center(
            child: BuildMaterialButton(
                roomCount: counterValueRoom,
                adultCount: counterValueAdults,
                childrenCount: counterValueChildren,
                ChildrenAgeList: agelist),
          ),
          SizedBox(
            height: 70,
          ),
          //  ListTile(
          //   title: new Container(height: 1,width: screenWidth(context,dividedBy: 1.5),color: Constants.kitGradients[2].withOpacity(.06),),
          //   onTap: () => {},
          // ),
        ],
      ),
    );
  }
}
