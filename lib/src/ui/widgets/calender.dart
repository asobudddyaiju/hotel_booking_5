import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_5/src/ui/screens/home_screens.dart';

import 'package:hotel_booking_5/src/ui/widgets/multi_date_range_picker.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/object_factory.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class DatePicker extends StatefulWidget {
  const DatePicker({Key key}) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
//   String dateFormatter(DateTime date) {
//   dynamic dayData =
//       '{ "1" : "Mon", "2" : "Tue", "3" : "Wed", "4" : "Thur", "5" : "Fri", "6" : "Sat", "7" : "Sun" }';

//   dynamic monthData =
//       '{ "1" : "Jan", "2" : "Feb", "3" : "Mar", "4" : "Apr", "5" : "May", "6" : "June", "7" : "Jul", "8" : "Aug", "9" : "Sep", "10" : "Oct", "11" : "Nov", "12" : "Dec" }';

//   return json.decode(dayData)['${date.weekday}'] +
//       ", " +
//       date.day.toString() +
//       " " +
//       json.decode(monthData)['${date.month}'] +
//       " " +
//       date.year.toString();
// }
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  List<List<DateTime>> intervals = [
    [
      DateTime.now().add(Duration(days: 0)),
      DateTime.now().add(Duration(days: 0)),
    ]
  ];

  @override
  void initState() {
    Intl.defaultLocale = Hive.box('locale').get(2) ?? 'en_US';
    initializeDateFormatting();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      backgroundColor: Colors.white,
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          GestureDetector(
            child: Text(
              "Done",
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Gilroy',
                  fontWeight: FontWeight.w300,
                  fontStyle: FontStyle.normal),
            ),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => HomeScreen()));
            },
          ),
          MaterialButton(
              disabledColor: Constants.kitGradients[0],
              height: 50,
              shape: CircleBorder(),
              child: Icon(
                Icons.check,
                size: 30,
                color: Colors.white,
              ),
              color: Constants.kitGradients[0],
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              }),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  child: Text(
                    "Cancel",
                    style: TextStyle(color: Colors.grey),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "Select dates",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Constants.kitGradients[7],
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'NexaLight'),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 98),
            ),
            Card(
              child: MultiDateRangePicker(
                initialValue: intervals,
                onChanged: (List<List<DateTime>> intervals) async {
                  print(
                      "CheckIn CheckOut Interval Days" + intervals.toString());
                  checkinDateCheck(intervals);
                },
                selectedDateTextColor: Colors.white,
                onlyOne: true,
                dateTextColor: Constants.kitGradients[0],
                buttonTextColor: Colors.white,
                primaryTextColor: Constants.kitGradients[0],
                selectionColor: Constants.kitGradients[0].withOpacity(.80),
                buttonColor: Constants.kitGradients[0],
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: [
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 16.0),
                          child: Container(
                            width: screenWidth(context, dividedBy: 1),
                            child: Column(
                              children: buildColumn1(),
                            ),
                          ),
                        ),
                      ),
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 16.0),
                          child: Container(
                            width: screenWidth(context, dividedBy: 1),
                            child: Column(
                              children: buildColumn2(),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  List<Widget> buildColumn1() {
    final List<Widget> list = [];

    for (final interval in intervals) {
      Hive.box('day').put(2, interval[1].weekday.toString());
      list.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Check-in Date",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Constants.kitGradients[7],
                fontSize: 20,
                fontWeight: FontWeight.w300,
                fontFamily: 'NexaLight'),
          ),
          Text(
            interval[0].day.toString() +
                " " +
                ObjectFactory()
                    .getMonthAlpha
                    .getCheckInMonth(interval[0].month.toString()) +
                " " +
                interval[0].year.toString(),
            style: TextStyle(
                color: Constants.kitGradients[9],
                fontWeight: FontWeight.w700,
                fontSize: 14.34,
                fontStyle: FontStyle.normal,
                fontFamily: 'NexaBold'),
          )
        ],
      ));

      if (interval != intervals.last)
        list.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Check-in Date",
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Constants.kitGradients[7],
                  fontSize: 20,
                  fontWeight: FontWeight.w300,
                  fontFamily: 'NexaLight'),
            ),
            Text(
              "checkin",
              style: TextStyle(
                  color: Constants.kitGradients[9],
                  fontWeight: FontWeight.w700,
                  fontSize: 14.34,
                  fontStyle: FontStyle.normal,
                  fontFamily: 'NexaBold'),
            )
          ],
        ));
    }

    return list;
  }

  List<Widget> buildColumn2() {
    final List<Widget> list = [];

    for (final interval in intervals) {
      Hive.box('day').put(1, interval[0].weekday.toString());
      list.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Check-out Date",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Constants.kitGradients[7],
                fontSize: 20,
                fontWeight: FontWeight.w300,
                fontFamily: 'NexaLight'),
          ),
          Text(
            interval[1].day.toString() +
                " " +
                ObjectFactory()
                    .getMonthAlpha
                    .getCheckInMonth(interval[1].month.toString()) +
                " " +
                interval[1].year.toString(),
            style: TextStyle(
                color: Constants.kitGradients[9],
                fontWeight: FontWeight.w700,
                fontSize: 14.34,
                fontStyle: FontStyle.normal,
                fontFamily: 'NexaBold'),
          )
        ],
      ));

      if (interval != intervals.last)
        list.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Check-in Date",
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Constants.kitGradients[7],
                  fontSize: 20,
                  fontWeight: FontWeight.w300,
                  fontFamily: 'NexaLight'),
            ),
            Text(
              "checkout",
              style: TextStyle(
                  color: Constants.kitGradients[9],
                  fontWeight: FontWeight.w700,
                  fontSize: 14.34,
                  fontStyle: FontStyle.normal,
                  fontFamily: 'NexaBold'),
            )
          ],
        ));
    }

    return list;
  }

  void showSnackMethod(String snackmsg) {
    final snackbar = SnackBar(
      content: Text(
        snackmsg,
        style: TextStyle(
            fontStyle: FontStyle.normal, fontFamily: 'Poppins', fontSize: 14.0),
      ),
      duration: Duration(seconds: 3),
      action: SnackBarAction(
        label: "OK",
        textColor: Colors.yellow,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => DatePicker()));
        },
      ),
      backgroundColor: Constants.kitGradients[0],
    );
    _globalKey.currentState.showSnackBar(snackbar);
  }

  void checkinDateCheck(List<List<DateTime>> intervals) {
    if (intervals[0][0].year > DateTime.now().year) {
      setState(() {
        this.intervals = intervals;
      });

      Hive.box('lang').put(
          5,
          intervals[0][0].year.toString() +
              '-' +
              intervals[0][0].month.toString() +
              '-' +
              intervals[0][0].day.toString());

      Hive.box('lang').put(
          6,
          intervals[0][1].year.toString() +
              '-' +
              intervals[0][1].month.toString() +
              '-' +
              intervals[0][1].day.toString());

      Hive.box('code').put(3, intervals[0][0].day.toString());
      Hive.box('code').put(5, intervals[0][0].month.toString());
      Hive.box('code').put(22, intervals[0][0].year.toString());
      Hive.box('code').put(6, intervals[0][1].month.toString());
      Hive.box('code').put(4, intervals[0][1].day.toString());
      Hive.box('code').put(23, intervals[0][1].year.toString());

      numberOfDays(intervals);
    }
    if (intervals[0][0].year < DateTime.now().year) {
      showSnackMethod('Please choose a valid Check-in date');
    }
    if (intervals[0][0].year == DateTime.now().year) {
      //for check month
      if (intervals[0][0].month < DateTime.now().month) {
        showSnackMethod('Please choose a valid Check-in date');
      }
      if (intervals[0][0].month == DateTime.now().month) {
        if (intervals[0][0].day >= DateTime.now().day) {
          setState(() {
            this.intervals = intervals;
          });

          Hive.box('lang').put(
              5,
              intervals[0][0].year.toString() +
                  '-' +
                  intervals[0][0].month.toString() +
                  '-' +
                  intervals[0][0].day.toString());

          Hive.box('lang').put(
              6,
              intervals[0][1].year.toString() +
                  '-' +
                  intervals[0][1].month.toString() +
                  '-' +
                  intervals[0][1].day.toString());

          Hive.box('code').put(3, intervals[0][0].day.toString());
          Hive.box('code').put(5, intervals[0][0].month.toString());
          Hive.box('code').put(22, intervals[0][0].year.toString());
          Hive.box('code').put(6, intervals[0][1].month.toString());
          Hive.box('code').put(4, intervals[0][1].day.toString());
          Hive.box('code').put(23, intervals[0][1].year.toString());

          numberOfDays(intervals);
          print('year,month and day are ok');
        } else {
          print('day is gone');
          showSnackMethod('Please choose a valid Check-in date');
        }
        print('month is this month');
      }
      //for upcoming month
      else if (intervals[0][0].month >= DateTime.now().month) {
        setState(() {
          this.intervals = intervals;
        });

        Hive.box('lang').put(
            5,
            intervals[0][0].year.toString() +
                '-' +
                intervals[0][0].month.toString() +
                '-' +
                intervals[0][0].day.toString());

        Hive.box('lang').put(
            6,
            intervals[0][1].year.toString() +
                '-' +
                intervals[0][1].month.toString() +
                '-' +
                intervals[0][1].day.toString());

        Hive.box('code').put(3, intervals[0][0].day.toString());
        Hive.box('code').put(5, intervals[0][0].month.toString());
        Hive.box('code').put(22, intervals[0][0].year.toString());

        Hive.box('code').put(6, intervals[0][1].month.toString());
        Hive.box('code').put(4, intervals[0][1].day.toString());
        Hive.box('code').put(23, intervals[0][1].year.toString());

        numberOfDays(intervals);
        print('month is upcoming month');
      }
      print("year is ok");
    }
  }
}
