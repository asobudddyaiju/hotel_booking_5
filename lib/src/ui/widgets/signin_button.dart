import 'package:flutter/material.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/utils.dart';

class SignButton extends StatefulWidget {
  @override
  _SignButtonState createState() => _SignButtonState();
}

class _SignButtonState extends State<SignButton> {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Constants.kitGradients[0],
      minWidth: screenWidth(context, dividedBy: 80),
      height: screenHeight(context, dividedBy: 20),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      child: Text(
        'Sign in',
        style: TextStyle(
            color: Colors.white, fontSize: screenWidth(context, dividedBy: 31)),
      ),
      onPressed: () {
        //Do something
      },
    );
  }
}
