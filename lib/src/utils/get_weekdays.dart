class GetDay {
  List<String> weekList = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

  String getWeekDay(String weekday) {
    String day;
    switch (weekday) {
      case "1":
        {
          day = weekList[0];
          break;
        }
      case "2":
        {
          day = weekList[1];

          break;
        }
      case "3":
        {
          day = weekList[2];

          break;
        }
      case "4":
        {
          day = weekList[3];

          break;
        }
      case "5":
        {
          day = weekList[4];

          break;
        }
      case "6":
        {
          day = weekList[5];

          break;
        }
      case "7":
        {
          day = weekList[6];

          break;
        }
    }
    return day;
  }
}
