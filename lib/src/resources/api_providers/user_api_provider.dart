import 'package:hotel_booking_5/src/models/destination_list_response.dart';
import 'package:hotel_booking_5/src/models/notification_list_response.dart';
import 'package:hotel_booking_5/src/models/state.dart';
import 'package:hotel_booking_5/src/utils/object_factory.dart';


class UserApiProvider {
  Future<State> sampleCall() async {
    final response = await ObjectFactory().apiClient.sampleApiCall();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<SampleResponseModel>.success(
          SampleResponseModel.fromJson(response.data));
    } else {
      return null;
    }
  }

  Future<State> destinationList({String searchKey}) async {
    final response =
        await ObjectFactory().apiClient.destinationList(searchKey: searchKey);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<DestinationListResponse>.success(
          DestinationListResponse.fromJson(response.data));
    } else {
      return null;
    }
  }
}
