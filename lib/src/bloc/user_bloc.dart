import 'dart:async';

import 'package:hotel_booking_5/src/models/destination_list_response.dart';
import 'package:hotel_booking_5/src/models/notification_list_response.dart';
import 'package:hotel_booking_5/src/models/state.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';
import 'package:hotel_booking_5/src/utils/object_factory.dart';
import 'package:hotel_booking_5/src/utils/validators.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class UserBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<SampleResponseModel> _sample =
      new StreamController<SampleResponseModel>.broadcast();
  StreamController<DestinationListResponse> _destination =
      new StreamController<DestinationListResponse>.broadcast();

//stream controller is broadcasting the  details

  Stream<SampleResponseModel> get sampleResponse => _sample.stream;

  Stream<DestinationListResponse> get destinationResponse =>
      _destination.stream;

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  sampleCall() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.sampleCall();

    if (state is SuccessState) {
      loadingSink.add(false);
      _sample.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _sample.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  destinationList({String searchKey}) async {
    loadingSink.add(true);

    State state =
    await ObjectFactory().repository.destinationList(searchKey: searchKey);

    if (state is SuccessState) {
      loadingSink.add(false);
      _sample.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _sample.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _sample?.close();
    _destination?.close();
  }
}

UserBloc userBlocSingle = UserBloc();
