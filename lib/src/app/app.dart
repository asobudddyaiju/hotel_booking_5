import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_5/localization/demo_local.dart';
import 'package:hotel_booking_5/src/ui/screens/splash_screen.dart';
import 'package:hotel_booking_5/src/utils/constants.dart';

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(locale);
    // MyApp.setLocale(context, locality);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

String lc;
String cc;
Locale _locale;
Locale locality;

class _MyAppState extends State<MyApp> {
  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  void initState() {
    setState(() {
      lc = Hive.box('locale').get(2);
      cc = Hive.box('locale').get(1);
    });

    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      // _locale = Locale(lc, cc) ?? Locale('en', 'US');
      if ((lc != null) && (cc != null)) {
        _locale = Locale(lc, cc);
      } else {
        _locale = Locale('en', 'US');
      }
    });
    super.didChangeDependencies();
  }

  // @override
  // void didChangeDependencies() {
  //   setState(() {
  //     _locale = Hive.box('locale').get(1);
  //   });

  //   super.didChangeDependencies();
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'Nexa',
              bodyColor: Constants.kitGradients[0],
              displayColor: Constants.kitGradients[0]),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        locale: _locale,
        localizationsDelegates: [
          DemoLocalizations
              .delegate, // ... app-specific localization delegate[s]
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocales) {
          for (var locale in supportedLocales) {
            if (locale.languageCode == deviceLocale.languageCode &&
                locale.countryCode == deviceLocale.countryCode) {
              return deviceLocale;
            }
          }
          return supportedLocales.first;
        },
        supportedLocales: [
          Locale('en', 'US'), // English
          Locale('ar', 'AE'), //Arabic
          Locale('bg', 'BG'), //Bulgarian
          // Locale('pt', 'PT'), //Brazil
          Locale('ca', 'CA'), //Catalan
          Locale('zh', 'CN'), //Chinese
          Locale('cs', 'CZ'), //Czech
          Locale('da', 'DK'), //Danish
          Locale('de', 'DE'), //German
          Locale('el', 'GR'), //Greek
          Locale('es', 'ES'), //Spanish
          Locale('et', 'EE'), //Estonian
          Locale('fi', 'FI'), //Finland
          Locale('fr', 'FR'), //France
          // Locale('zh', 'CN'), //Hongkong
          Locale('hr', 'HR'), //Croatian
          Locale('hu', 'HU'), //Hungarian
          Locale('id', 'ID'), //Indonesian
          Locale('he', 'IL'), //Israel
          Locale('is', 'IS'), //Iceland
          Locale('it', 'IT'), //Italy
          Locale('ja', 'JP'), //Japanese
          Locale('ko', 'KO'), //Korea
          Locale('lt', 'LT'), //Lithuanian
          Locale('lv', 'LV'), //Latvian
          Locale('ms', 'MY'), //Malaysia
          Locale('nl', 'NL'), //Netherlands
          Locale('no', 'NO'), //Norwegian
          Locale('pl', 'PL'), //Poland
          Locale('pt', 'PT'), //Portugal
          Locale('ro', 'RO'), //Romanian
          Locale('ru', 'RU'), //Russian
          Locale('sk', 'SK'), //Slovak
          Locale('sl', 'SL'), //Slovenian
          Locale('sr', 'SP'), //Serbian
          Locale('sv', 'SE'), //Sweden
          Locale('th', 'TH'), //Thai
          Locale('tl', 'PH'), //Philippines
          Locale('tr', 'TR'), //Turkish
          // Locale('zh', 'CN'), //Chinese(Traditional)
          Locale('uk', 'UA'), //Ukrainian
          Locale('vi', 'VN'), //Vietnamese
        ],
        home: FutureBuilder(
            future: Hive.openBox('lang'),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError)
                  return Text(snapshot.error.toString());
                else
                  return SplashScreen();
              } else
                return Scaffold();
            }));
  }

  // void didChangeDependencies() {
  //   setState(() {
  //     this._locale = locality;
  //   });
  //   print(locality.languageCode);
  //   super.didChangeDependencies();
  // }

  @override
  void dispose() {
    Hive.box('adult').clear();
    Hive.close();
    super.dispose();
  }
}
